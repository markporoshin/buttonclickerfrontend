import {Navbar} from 'react-bootstrap'
import './style.css'

function Header() {
    return (
        <Navbar expand="lg" bg="dark" variant="dark">
            <Navbar.Brand>
                <div>
                    Счетчик запуска лазеров
                </div>
            </Navbar.Brand>
        </Navbar>
    )
}

export default Header;