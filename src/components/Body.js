import uuid from 'react-uuid'
import axios from 'axios';
import {Row, Col} from 'react-bootstrap';
import React, { useState, useEffect } from 'react';

import './style.css'

function Body() {
    const [counter, setCounter] = useState(0)
    const [token, setToken] = useState(null)
    
    useEffect(()=>{
        var localToken = localStorage.getItem('token')
        if (localToken == null || localToken === 'undefined') {
            localToken = uuid()
            localStorage.setItem('token', localToken)
            console.log('create new token')
        }
        console.log('token' + localToken)
        let user = {
            token: localToken
        }
        axios.post('http://localhost:8090/user', user).then(
            responce => {
                console.log('success send token:' +  responce)
                setCounter(responce.data.counter)
                setToken(localToken)
            }
        ).catch(e => {
            console.log('error: ' + e)
        })
    }, [])

    const onClick = () => {
        let user = {
            token: token
        }
        axios.post('http://localhost:8090/user/increment', user).then(
            responce => {
                console.log('success increment:' +  responce)
                setCounter(responce.data.counter)
            }
        ).catch(e => {
            console.log('error: ' + e)
        })
    }

    return (
        <Row>
            <Col>
                <button className="center orange-button" onClick={()=>onClick()}>
                    Активировать лазеры
                </button>
            </Col>
            <Col>
                <div className="center center-text">
                    количество запусков: {counter}
                </div>
            </Col>
        </Row>
    )
}

export default Body;
